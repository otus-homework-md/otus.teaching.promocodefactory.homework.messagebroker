﻿using Common.Options;
using MassTransit;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extesion
{
    public static class RabbitMQExt
    {
        public static IRabbitMqBusFactoryConfigurator ConfigureHost(this IRabbitMqBusFactoryConfigurator configurator)
        {
            var rabbitMqOptions = CommonConfigurationManager.Configuration.GetSection(RabbitMQOpt.Position).Get<RabbitMQOpt>();

            configurator.Host(rabbitMqOptions.Host, rabbitMqOptions.VirtualHost, hostCfg =>
            {
                hostCfg.Username(rabbitMqOptions.UserName);
                hostCfg.Password(rabbitMqOptions.Password);
            });

            return configurator;
        }
    }
}
