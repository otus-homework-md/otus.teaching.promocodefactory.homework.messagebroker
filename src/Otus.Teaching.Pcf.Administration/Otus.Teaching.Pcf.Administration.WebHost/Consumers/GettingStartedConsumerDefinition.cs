﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Contsumers;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class GettingStartedConsumerDefinition :
       ConsumerDefinition<GettingStartedConsumer>
    {
        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<GettingStartedConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseMessageRetry(r => r.Intervals(500, 1000));
        }
    }
}
