namespace Otus.Teaching.Pcf.Administration.WebHost.Contsumers
{
    using System.Threading.Tasks;
    using MassTransit;
    using Microsoft.Extensions.Logging;
    using Common.Contracts;
    using Otus.Teaching.Pcf.Administration.Core.Infrastructure;

    public class GettingStartedConsumer :
        IConsumer<GivePromoCodeToCustomerRecord>
    {
        readonly ILogger<GettingStartedConsumer> _logger;
        readonly IUpdaterAppliedPromocodes _updaterAppliedPromocodes;

        public GettingStartedConsumer(ILogger<GettingStartedConsumer> logger,IUpdaterAppliedPromocodes updaterAppliedPromocodes)
        {
            _logger = logger;
            _updaterAppliedPromocodes = updaterAppliedPromocodes;
        }
        public Task Consume(ConsumeContext<GivePromoCodeToCustomerRecord> context)
        {
            _logger.LogInformation("PromoCode - {PromoCode}", context.Message.PromoCode);
            var res=_updaterAppliedPromocodes.UpdateAppliedPromocodesAsync(context.Message.PartnerManagerId);
            return Task.CompletedTask;
        }
    }
}