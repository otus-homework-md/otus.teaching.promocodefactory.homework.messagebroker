﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.Administration.Core.Infrastructure;
using System.Threading.Tasks;
using System;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.WebHost.Controllers;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class UpdateAppliedPromocodesService: IUpdaterAppliedPromocodes
    {
        private readonly IRepository<Employee> _employeeRepository;
       
        public UpdateAppliedPromocodesService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;

        }

        public async Task<bool> UpdateAppliedPromocodesAsync(Guid? id)
        {
            if (id==null) return false;

            var employee = await _employeeRepository.GetByIdAsync(id?? Guid.Empty);

            if (employee == null)
                return false;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}
