﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Infrastructure
{
    public interface IUpdaterAppliedPromocodes
    {
        Task<bool> UpdateAppliedPromocodesAsync(Guid? id);
    }
}
