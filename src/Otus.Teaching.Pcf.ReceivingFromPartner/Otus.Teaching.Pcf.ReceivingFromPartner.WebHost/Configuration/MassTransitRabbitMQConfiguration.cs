﻿using MassTransit;
using Common.Extesion;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Configuration
{
    public static class MassTransitRabbitMQConfiguration
    {
        public static IServiceCollection AddMassTransitRabbitMQ(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddMassTransit(c =>
            {
                c.UsingRabbitMq((context, cfg) =>
                {
                    cfg.ConfigureHost();
                    cfg.UseRawJsonSerializer();
                });
            });

            return serviceCollection;
        }
    }
}
