﻿using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Common.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromocodeToCustomerBaseOnMT : IGivingPromoCodeToCustomerBaseOnMT
    {


        private readonly IBus _bus;

        private readonly IPublishEndpoint _endPoint;

        public GivingPromocodeToCustomerBaseOnMT(IBus bus, IPublishEndpoint endPoint)
        {
            _bus = bus;
            _endPoint = endPoint;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerRecord()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };



            await _endPoint.Publish<GivePromoCodeToCustomerRecord>(dto);
        }

    }
}
